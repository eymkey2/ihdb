paypal = Meteor.npmRequire('paypal-rest-sdk');

paypal.configure({
  'mode': 'sandbox',
  'client_id': Meteor.settings.public.paypal,
  'client_secret': Meteor.settings.private.paypal
});

ServiceConfiguration.configurations.upsert(
  { service: "paypal" },
  { 
  	$set: 
	  	{ 
		  	clientId: Meteor.settings.public.paypal,
		  	secret: Meteor.settings.private.paypal,
		  	loginStyle: "popup",
		  	environment: "sandbox"
		  }
	}
);





// publish information about the logged in user
Meteor.publish( 'user', function () {

	return Meteor.users.find(this.userId, {fields: {'services': 1}});
});

// publish own hotels
Meteor.publish( 'hotels', function () {
	if( this.userId ){
		return Hotels.find( {owner: this.userId} );
	}
	else {
		return this.ready();
	}
});

// publish own favorites
Meteor.publish( 'favorites', function () {
	if( this.userId ){
		return Favorites.find( {owner: this.userId} );
	}
	else {
		return this.ready();
	}
});

// publish information about a specific hotel
Meteor.publish( 'hotel', function (hid) {

	Meteor.call( 'isLoggedIn', this.userId );
	var hotel = Meteor.call( 'doesHidExist', hid );
	
	if( hotel.owner === this.userId ){

		return [
			Hotels.find( {hid: hid} ),
			Rooms.find( {owner: hid} )
		];

	}
	else {
		Meteor.call( 'isHidPubliclyAvailable', hid );

		return [
			Hotels.find( {hid: hid} ),
			Rooms.find( {owner: hid, public: true} )
		];
	}

});