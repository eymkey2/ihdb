Meteor.methods({

	/*--------------*\
			
			DEFINITION

	\*--------------*/

	/*

		private: only the owner of the hotel can do this type of operations

		public: all logged in users can do this type of operations

	*/





	/*----------*\
			
			HOTEL

	\*----------*/


  //
	//  private
	//

	// inserts a new hotel to the database
	// returns true or throws an error
	addHotel: function ( doc ) {
		check( doc, Object);
		check( doc.hid, String);

		Meteor.call( 'isLoggedIn', this.userId );

		doc.hid = doc.hid.toLowerCase();
		doc.blocked = false;
		doc.createdAt = new Date();
		doc.owner = this.userId;
		
		Meteor.call( 'isHidAvailable', doc.hid );

		check( doc, Schemas.hotel);
		return Hotels.insert( doc, function ( error, id ) {
			if ( error ) {
				throw new Meteor.Error( 'internal server error: '+error, 'addHotel' );
			}
			else {
				return true;
			}
		});
	},

	// update hotel information
	// returns true or throws an error
	updateHotel: function ( doc ) {
		check( doc, Schemas.updateHotel );
		var hid = doc.hid;

		delete doc.hid;
		delete doc.blocked;
		delete doc.createdAt;
		delete doc.owner;

		Meteor.call( 'isOwner', hid, this.userId );

		Hotels.update( {hid: hid}, {$set: doc}, function ( error, num ) {
			if ( error ) {
				throw new Meteor.Error( 'internal server error: '+error, 'updateHotel' );
			}
			else {
				return true;
			}
		});
	},

	// remove a hotel from the database
	// returns true or throws an error
	// TODO: remove HOTEL from favorites
	removeHotel: function ( doc ) {
		check( doc, Schemas.removeHotel );

		Meteor.call( 'isOwner', doc.hid, this.userId );

		var rooms = Meteor.call( 'getRoomsByHid', {hid: doc.hid} );
		rooms.forEach( function (room) {
			Meteor.call( 'removeRoom', {rid: room._id});
		});

		Hotels.remove({hid: doc.hid}, function ( error ) {
			if ( error ) {
				throw new Meteor.Error('internal server error: '+error, 'removeHotel');
			}
			else {
				return true;
			}
		});
	},


	//
	//  public
	//

	// get a hotel from the database by its hid
	// returns the database entry or throws a error
	getHotelByHid: function ( doc ) {
		check( doc, {hid: String} );

		Meteor.call( 'isLoggedIn', this.userId );
		return Meteor.call( 'isHidPubliclyAvailable', doc.hid );
	},

	getHidById: function (doc) {
		check( doc, {id: String} );

		var hotel = Hotels.findOne(doc.id);
		return hotel.hid;
	},





	/*----------*\
			
			ROOM

	\*----------*/


	//
	//  private
	//

	// add a new room to a hotel
	// returns true or thorws an error
	addRoom: function ( doc ) {
		check( doc, Schemas.room );

		var owner = Meteor.call( 'isOwner', doc.owner, this.userId );

		Rooms.insert( doc, function ( error, id ) {
			if ( error ) {
				throw new Meteor.Error('internal server error: '+error, 'addRoom');
			}
			else {
				return true;
			}
		});
	},

	// update room
	// return true or throws an error
	updateRoom: function ( doc ) {
		check( doc, Schemas.updateRoom );

		var room = Meteor.call( 'doesRidExist', doc.rid );
		Meteor.call( 'isOwner', room.owner, this.userId );

		Rooms.update( {_id: doc.rid}, {$set: doc}, function ( error, num ) {
			if ( error ) {
				throw new Meteor.Error( 'internal server error: '+error, 'updateRoom' );
			}
			else {
				return true;
			}
		});
	},

	// remove room
	// returns true or throws an error
	//TODO: remove bookings
	removeRoom: function ( doc ) {
		check( doc, Schemas.removeRoom );

		var room = Meteor.call( 'doesRidExist', doc.rid );
		Meteor.call( 'isOwner', room.owner, this.userId );

		//
		//	TODO: CHECK IF THERE ARE OPEN BOOKINGS
		//				AND DENY DELETE OPERATION
		//

		Rooms.remove( {_id: doc.rid}, function ( error ) {
			if (error ) {
				throw new Meteor.Error( 'internal server error: '+error, 'removeRoom' );
			}
			else {
				return true;
			}
		});
	},


	//
	//  public
	//

	// get room by rid
	// returns the database entry or throws an error
	getRoomByRid: function ( doc ) {
		check( doc, {rid: String} );

		Meteor.call( 'isLoggedIn', this.userId );
		return Meteor.call( 'isRidPubliclyAvailable', doc.rid );
	},

	// get rooms by hid
	// returns an array of the database entries or thorws an error
	getRoomsByHid: function ( doc ) {
		check( doc, Schemas.getRoomsByHid );

		Meteor.call( 'isLoggedIn', this.userId );
		
		var hotel = Meteor.call('doesHidExist', doc.hid);

		if( hotel.owner === this.userId ){
			return Rooms.find( {owner: doc.hid} ).fetch();
		}
		else{
			Meteor.call( 'isHidPubliclyAvailable', doc.hid );
			return Rooms.find( {owner: doc.hid, public: true} ).fetch();
		}
	},





	/*-------------*\
			
			FAVORITE

	\*-------------*/

	addFavorite: function (doc) {
		check(doc, {hid: String});

		Meteor.call('isLoggedIn', this.userId);
		var hotel = Meteor.call('doesHidExist', doc.hid);

		var doc = {
			hid: hotel.hid,
			name: hotel.name,
			owner: this.userId
		}

		var favorite = Favorites.findOne({hid: doc.hid, owner: this.userId});

		if( !favorite ){
			Favorites.insert( doc, function ( error, id ) {
				if ( error ) {
					throw new Meteor.Error('internal server error: '+error, 'addFavorite');
				}
				else {
					return true;
				}
			});
		}
		else {
			throw new Meteor.Error('favorite exists', 'addFavorite');
		}
	},

	removeFavorite: function (doc) {
		check(doc, {hid: String});

		Meteor.call('isLoggedIn', this.userId);
		var favorites = Favorites.find({hid: doc.hid, owner: this.userId});

		if( favorites.fetch() ){

			favorites.forEach(function(favorite){
				Favorites.remove( favorite._id, function ( error ) {
					if ( error ) {
						throw new Meteor.Error('internal server error: '+error, 'removeFavorite');
					}
					else {
						return true;
					}
				});
			});
			
		}
	},





	/*-------------*\
			
			BOOKINGS

	\*-------------*/

	//
	//	public
	//

	// add booking to the database
	// returns the id of the database entry or throws an error
	addBooking: function ( doc, creditCard) {
		check( doc, Schemas.addBooking );

		var hotel = Meteor.call('doesHidExist', doc.hid);
		delete doc.hid;

		//check availbility
		doc.rooms.forEach(function (room){
			Meteor.call( 'isRoomAvailable', {
				rid: room.rid,
				checkIn: doc.checkIn,
				checkOut: doc.checkOut,
				quantity: room.quantity,
				returnRoomsAvailable: false
			});
		});
		
		//create payment
		var payment = {
			"intent": "sale",
		  "payer": {
		  	"payment_method": "credit_card",
		    "funding_instruments": [{
		        "credit_card": creditCard
		    }]
		  },
		  "transactions": [{
		    "amount": {
		    	"total": Meteor.call( 'getPriceForBooking', doc ),
		      "currency": hotel.currency,
		    },
		    "description": "This is the payment transaction description."
		  }]
		};

		console.log(JSON.stringify(payment, null, 2));

		doc.pid = Meteor.call( 'addPayment', payment, hotel );
		doc.createdAt = new Date();

		check(doc, Schemas.booking);
		return Bookings.insert( doc );
	},

	// checks if the room is available at the given time periode
	// return the id of the booking or throws an error

	// TODO: check for invalid dates
	isRoomAvailable: function ( doc ) {
		check( doc, 
			{
				rid: String,
				checkIn: Date,
				checkOut: Date,
				quantity: Match.OneOf(Number, undefined),
				returnRoomsAvailable: Boolean
			}
		);

		doc.checkIn = moment(doc.checkIn);
		doc.checkOut = moment(doc.checkOut);

		if( (!doc.checkIn.isValid()) || (!doc.checkOut.isValid()) ){
			throw new Meteor.Error('dates must be valid', 'isRoomAvailable');
		}
		if ( !doc.checkIn.isBefore(doc.checkOut) ) {
			throw new Meteor.Error('checkIn must be before checkOut', 'isRoomAvailable');
		}
		if( !moment().isBefore(doc.checkIn) ){
			throw new Meteor.Error('checkIn must be in the future', 'isRoomAvailable');
		}

		// doing some magic
		// begin magic
		var room = Meteor.call( 'isRidPubliclyAvailable', doc.rid );
		var bookings = Meteor.call( 'getBookingsByRid', {rid: doc.rid} );

		if ( doc.quantity > room.amount ){
			throw new Meteor.Error('there are not enough rooms', 'isRoomAvailable');
		}

		var booked = bookings.reduce(function(all, booking){
			for(var i = moment(booking.checkIn); i.isBefore(booking.checkOut); i.add(1,'d')){
				if (all[i.format('DD MM YY')]){

					booking.rooms.forEach( function(room) {
						if (room.rid === doc.rid) {
							all[i.format('DD MM YY')] += room.quantity;
						}
					});
					
				}
				else {

					booking.rooms.forEach( function(room) {
						if (room.rid === doc.rid) {
							all[i.format('DD MM YY')] = room.quantity;
						}
					});

				}
			}
			return all;
		}, {});


		var selectedPeriod = Meteor.call('getSelectedPeriod', {checkIn: doc.checkIn.toDate(), checkOut: doc.checkOut.toDate()});

		if( doc.returnRoomsAvailable ){
			var roomsAvailable = selectedPeriod.reduce(function(all, item, index){
				all[item] = room.amount;
				return all;
			}, {});
		}

		for ( var date in booked ){
			for ( var i = 0; i < selectedPeriod.length; i++ ) {
				if ( date === selectedPeriod[i] ) {
					if( doc.returnRoomsAvailable ) {
						roomsAvailable[date] -= booked[date];
						continue;
					}
					if ( booked[date] > (room.amount - doc.quantity) ) {
						throw new Meteor.Error( 'not available!', 'isRoomAvailable' );
					}
				}
			}
		}
		// end magic

		return doc.returnRoomsAvailable ? roomsAvailable : true;
	},

	// calculates the price for a booking
	// returns the price in cents
	getPriceForBooking: function ( doc ) {
		check( doc,
			{
				rooms: [{
					rid: String,
					quantity: Number
				}],
				checkIn: Date,
				checkOut: Date
			}
		);

		var selectedPeriod  = Meteor.call( 'getSelectedPeriod', {checkIn: doc.checkIn, checkOut: doc.checkOut} );
		var total = 0;

		doc.rooms.forEach( function (room) {
			total += getPrice(room.rid, room.quantity);
		});

		function getPrice(rid, quantity){
			var room = Meteor.call( 'isRidPubliclyAvailable', rid );
			var feesInPercent = 0;
			var feesInCents = 0;

			return Math.ceil( (room.defaultPrice*selectedPeriod.length*quantity) * (100 / (100 - feesInPercent)) + feesInCents );
		}

		return total.toFixed(2);
	},

	getSelectedPeriod: function ( doc ) {
		check( doc, {
			checkIn: Date,
			checkOut: Date
		});

		var selectedPeriod  = [];
		for(var i = moment(doc.checkIn); i.isBefore(doc.checkOut); i.add(1,'d')){
			selectedPeriod.push(i.clone().format('DD MM YY'));
		}

		return selectedPeriod;
	},

	//
	//  private
	//
	// TODO: only the owner of the hotels and the SERVER can see the bookings

	getBookingsByHid: function (doc) {
		check(doc, {hid: String});

		var rooms = Meteor.call('getRoomsByHid', {hid: doc.hid});
		var bookings = [];

		for (var i = 0; i < rooms.length; i++) {
			var booking = Meteor.call('getBookingsByRid', {rid: rooms[i]._id});
			bookings.push(booking);
		}

		return bookings;
	},

	getBookingsByRid: function ( doc ) {
		check( doc, {rid: String} );

		var room = Meteor.call( 'doesRidExist', doc.rid );
		return Bookings.find( {'rooms.rid': {$in: [room._id]}} ).fetch();
	},


	/*-------------*\
			
			PAYMENTS

	\*-------------*/

	// execute a payment with stipe and adds a new entry in the database
	// returns true or throws an error
	
	addPayment: function ( create_payment, hotel ) {

		var me = Meteor.user();
		var owner = Meteor.users.findOne(hotel.owner);

		var paymentCreate = Meteor.wrapAsync(paypal.payment.create, paypal.payment);
		var saleGet = Meteor.wrapAsync(paypal.sale.get, paypal.sale);
		var payoutCreate = Meteor.wrapAsync(paypal.payout.create, paypal.payout);

		var payment = paymentCreate(create_payment);

		if(payment.transactions[0].related_resources[0].sale.state === 'completed') {

			var sale = saleGet(payment.transactions[0].related_resources[0].sale.id);
			var total = Number(sale.amount.total) - Number(sale.transaction_fee.value);

				var create_payout = {
			    "sender_batch_header": {
		        "sender_batch_id": payment.id,
		        "email_subject": "You have a payment"
			    },
			    "items": [
		        {
	            "recipient_type": "EMAIL",
	            "amount": {
	                "value": (total * (Number(hotel.commission) / 100)).toFixed(2),
	                "currency": hotel.currency
	            },
	            "receiver": me.services.paypal.email
		        },
		        {
	            "recipient_type": "EMAIL",
	            "amount": {
	                "value": (total * (1 - Number(hotel.commission) / 100)).toFixed(2),
	                "currency": hotel.currency
	            },
	            "receiver": owner.services.paypal.email
		        }
			    ]
				};

				var payout = payoutCreate(create_payout);
				console.log('finished');
			  return Payments.insert( {by: me.id, payment: payment} );
    }
    else {
    	throw new Meteor.Error('payment is not completed!', 'addPayment');
    }
	},
	

	/*-------------*\
			
			UTILITIES

	\*-------------*/

	// checks if the user is logged in
	// returns true or throws an error
	isLoggedIn: function ( userId ) {
		check( userId, Match.OneOf(String, null) );

		if ( userId ) {
			return true;
		}
		else {
			throw new Meteor.Error( 'fobidden', 'isLoggedIn' );
		}
	},

	// checks if the passed hid exists
	// returns the database entry or throws an error
	doesHidExist: function ( hid ) {
		check( hid, String );

		var hotel = Hotels.findOne( {hid: hid} );

		if ( hotel ) {
			return hotel;
		}
		else {
			throw new Meteor.Error( 'not found', 'doesHidExist' );
		}
	},

	// checks if the passed rid exists
	// returns the database entry or throws an error
	doesRidExist: function ( rid ) {
		check( rid, String );

		var room = Rooms.findOne( {_id: rid} );

		if ( room ) {
			return room;
		}
		else {
			throw new Meteor.Error( 'not found', 'doesRidExist' );
		}
	},

	// checks if the passed hid is available
	// returns true or throws an error
	isHidAvailable: function ( hid ) {
		check( hid, String );

		var hotel = Hotels.findOne( {hid: hid} );

		if ( hotel ) {
			throw new Meteor.Error('hid is taken', 'isHidAvailable');
		}
		else {
			return true;
		}
	},

	// checks if the passed hid is publicly available
	// returns the database entry or throws an error
	isHidPubliclyAvailable: function ( hid ) {
		check( hid, String );

		var hotel = Meteor.call( 'doesHidExist', hid );

		if ( hotel && hotel.public && !hotel.blocked ) {
			return hotel;
		}
		else {
			throw new Meteor.Error( 'not publicly available', 'isHidPubliclyAvailable' );
		}
	},

	// checks if the hid of the passed rid is publicly available or if the owner of the hid is makeing the request
	// returns the database entry or throws an error
	isRidPubliclyAvailable: function ( rid ) {
		check(rid, String);

		var room = Meteor.call( 'doesRidExist', rid );

		if ( room.public ) {
			Meteor.call( 'isHidPubliclyAvailable', room.owner );
			return room;
		}
		else {
			throw new Meteor.Error( 'room is not publicly available', 'isRidPubliclyAvailable' );
		}
	},

	// checks if the passed user is the owner of the passed hid
	// returns true or throws a meteor error
	isOwner: function ( hid, userId ) {
		check( hid, String );
		check( userId, Match.OneOf(String, null) );

		Meteor.call( 'isLoggedIn', userId );
		var hotel = Meteor.call( 'doesHidExist', hid );

		if ( hotel && hotel.owner === userId ) {
			return true;
		}
		else {
			throw new Meteor.Error( 'forbidden', 'isOwner' );
		}
	}

});