//
// collections
//

Hotels = new Mongo.Collection('hotels');
Rooms = new Mongo.Collection('rooms');
Bookings = new Mongo.Collection('bookings');
Payments = new Mongo.Collection('payments');
Favorites = new Mongo.Collection('favorites');

Hotels.allow({
    insert: function(){return false},
    update: function(){return false},
    remove: function(){return false}
});

Rooms.allow({
    insert: function(){return false},
    update: function(){return false},
    remove: function(){return false}
});

Bookings.allow({
    insert: function(){return false},
    update: function(){return false},
    remove: function(){return false}
});

Payments.allow({
    insert: function(){return false},
    update: function(){return false},
    remove: function(){return false}
});

Favorites.allow({
    insert: function(){return false},
    update: function(){return false},
    remove: function(){return false}
});

Meteor.users.allow({
    insert: function(){return false},
    update: function(){return false},
    remove: function(){return false}
});



//
// schemas
Schemas = {};

// user
Schemas.user = new SimpleSchema({
    profile: {
        type: Object,
        optional: true
    },
    services: {
        type: Object,
        blackbox: true
    },
    createdAt: {
        type: Date,
        autoValue: function(){
            return new Date();
        },
        optional: true
    },
});

// hotel
Schemas.hotel = new SimpleSchema({
	'name': {
		type: String
	},
	'hid': {
		type: String,
        regEx: /^[a-zA-Z0-9]+$/,
        denyUpdate: true,
		min: 8,
		max: 8
	},
    'description': {
        type: String,
        optional: true
    },
    'rating': {
        type: Number,
        min: 1,
        max: 5
    },
    'commission': {
        type: Number,
        min: 8,
        max: 50
    },
    'currency': {
        type: String,
        allowedValues: ['EUR', 'TRY']
    },

	'location': {
		type: Object,
		blackbox: false
	},
	'location.country': {
		type: String,
		label: 'country'
	},
    'location.state': {
        type: String,
        label: 'state'
    },
    'location.city': {
        type: String,
        label: 'city'
    },
    'location.zip': {
        type: Number,
        label: 'zip',
        min: 999,
        max: 999999999
    },
    'location.address': {
        type: String,
        label: 'address'
    },

	'contact': {
		type: Object,
		blackbox: false
	},
	'contact.website': {
		type: String,
		regEx: SimpleSchema.RegEx.Domain,
		label: 'website'
	},
	'contact.email': {
		type: String,
		regEx: SimpleSchema.RegEx.Email,
		label: 'email'
	},
	'contact.phone': {
		type: String,
		label: 'phone'
	},

    'public': {
        type: Boolean
    },
    'blocked': {
        type: Boolean
    },
	'createdAt': {
		type: Date,
        denyUpdate: true
	},
    'owner': {
        type: String,
        denyUpdate: true
    }
});

// room
Schemas.room = new SimpleSchema({
    'name': {
        type: String
    },
    'size': {
        type: Number,
        label: 'number of persons',
        min: 1,
        max: 1000
    },
    'amount': {
        type: Number,
        label: 'number of rooms of this type',
        min: 1,
        max: 1000
    },
    'defaultPrice': {
        type: Number,
        min: 1,
        max: 10000
    },
    'owner': {
        type: String,
        regEx: /^[a-zA-Z0-9]+$/,
        denyUpdate: true,
        min: 8,
        max: 8
    },
    'public': {
        type: Boolean
    }
});

// booking
Schemas.booking = new SimpleSchema({
    rooms: {
        type: [
            new SimpleSchema({
                'rid': {
                    type: String,
                    regEx: SimpleSchema.RegEx.Id
                },
                'quantity': {
                    type: Number,
                    min: 1
                }
            })
        ]
    },
    'checkIn': {
        type: Date
    },
    'checkOut': {
        type: Date
    },
    'pid': {
        type: String,
        denyUpdate: true
    },
    'createdAt': {
        type: Date,
        denyUpdate: true
    }
});

// payments
Schemas.payment = new SimpleSchema({
    'by': {
        type: String,
        denyUpdate: true,
        optional: true
    },
    'payment': {
        type: Object,
        denyUpdate: true,
        blackbox: true
    }
});

// favorites
Schemas.favorites = new SimpleSchema({
    'hid': {
        type: String
    },
    'name': {
        type: String
    },
    'owner': {
        type: String
    }
});



//
//methods

//hotel
Schemas.addHotel = new SimpleSchema({
    'name': {
        type: String,
        label: 'name'
    },
    'hid': {
        type: String,
        label: 'hid',
        regEx: /^[a-zA-Z0-9]+$/,
        min: 8,
        max: 8
    },
    'description': {
        type: String,
        label: 'description',
        optional: true
    },
    'rating': {
        type: Number,
        label: 'rating',
        min: 1,
        max: 5
    },
    'commission': {
        type: Number,
        label: 'commission',
        min: 8,
        max: 50
    },
    'currency': {
        type: String,
        allowedValues: ['EUR', 'TRY']
    },

    'location': {
        type: Object,
        blackbox: false
    },
    'location.country': {
        type: String,
        label: 'country'
    },
    'location.state': {
        type: String,
        label: 'state'
    },
    'location.city': {
        type: String,
        label: 'city'
    },
    'location.zip': {
        type: Number,
        label: 'zip',
        min: 999,
        max: 999999999
    },
    'location.address': {
        type: String,
        label: 'address'
    },

    'contact': {
        type: Object,
        blackbox: false
    },
    'contact.website': {
        type: String,
        regEx: SimpleSchema.RegEx.Domain,
        label: 'website'
    },
    'contact.email': {
        type: String,
        regEx: SimpleSchema.RegEx.Email,
        label: 'email'
    },
    'contact.phone': {
        type: String,
        label: 'phone'
    },

    'public': {
        type: Boolean
    }
});

Schemas.updateHotel = new SimpleSchema({
    'name': {
        type: String
    },
    'hid': {
        type: String,
        regEx: /^[a-zA-Z0-9]+$/,
        min: 8,
        max: 8
    },
    'description': {
        type: String,
        optional: true
    },
    'rating': {
        type: Number,
        min: 1,
        max: 5
    },
    'commission': {
        type: Number,
        min: 8,
        max: 50
    },
    'currency': {
        type: String,
        allowedValues: ['EUR', 'TRY']
    },

    'location': {
        type: Object,
        blackbox: false
    },
    'location.country': {
        type: String,
        label: 'country'
    },
    'location.state': {
        type: String,
        label: 'state'
    },
    'location.city': {
        type: String,
        label: 'city'
    },
    'location.zip': {
        type: Number,
        label: 'zip',
        min: 999,
        max: 999999999
    },
    'location.address': {
        type: String,
        label: 'address'
    },

    'contact': {
        type: Object,
        blackbox: false
    },
    'contact.website': {
        type: String,
        regEx: SimpleSchema.RegEx.Domain,
        label: 'website'
    },
    'contact.email': {
        type: String,
        regEx: SimpleSchema.RegEx.Email,
        label: 'email'
    },
    'contact.phone': {
        type: String,
        label: 'phone'
    },

    'public': {
        type: Boolean
    }
});

Schemas.removeHotel = new SimpleSchema({
    'hid': {
        type: String,
        regEx: /^[a-zA-Z0-9]+$/,
        min: 8,
        max: 8
    }
});

//room
Schemas.addRoom = new SimpleSchema({
    'name': {
        type: String
    },
    'size': {
        type: Number,
        label: 'number of persons',
        min: 1,
        max: 1000
    },
    'amount': {
        type: Number,
        label: 'number of rooms of this type',
        min: 1,
        max: 1000
    },
    'defaultPrice': {
        type: Number,
        min: 1,
        max: 10000
    },
    'owner': {
        type: String,
        regEx: /^[a-zA-Z0-9]+$/,
        min: 8,
        max: 8,
        optional: true
    },
    'public': {
        type: Boolean
    }
});

Schemas.updateRoom = new SimpleSchema({
    'rid': {
        type: String,
        regEx: SimpleSchema.RegEx.Id
    },

    'name': {
        type: String
    },
    'size': {
        type: Number,
        label: 'number of persons',
        min: 1,
        max: 1000
    },
    'amount': {
        type: Number,
        label: 'number of rooms of this type',
        min: 1,
        max: 1000
    },
    'defaultPrice': {
        type: Number,
        min: 1,
        max: 10000
    },
    'public': {
        type: Boolean
    }
});

Schemas.removeRoom = new SimpleSchema({
    'rid': {
        type: String,
        regEx: SimpleSchema.RegEx.Id
    }
});

Schemas.getRoomsByHid = new SimpleSchema({
    'hid': {
        type: String,
        regEx: /^[a-zA-Z0-9]+$/,
        min: 8,
        max: 8
    }
});

// booking
Schemas.addBooking = new SimpleSchema({
    rooms: {
        type: [
            new SimpleSchema({
                'rid': {
                    type: String,
                    regEx: SimpleSchema.RegEx.Id
                },
                'quantity': {
                    type: Number,
                    min: 1
                }
            })
        ]
    },
    'hid': {
        type: String,
        label: 'hid',
        regEx: /^[a-zA-Z0-9]+$/,
        min: 8,
        max: 8
        
    },
    'checkIn': {
        type: Date
    },
    'checkOut': {
        type: Date
    }
});

Schemas.getBookingsByRid = new SimpleSchema({
    'rid': {
        type: String
    }
});



//
// attachments
Hotels.attachSchema(Schemas.hotel);
Rooms.attachSchema(Schemas.room);
Bookings.attachSchema(Schemas.booking);
Payments.attachSchema(Schemas.payment);
Favorites.attachSchema(Schemas.favorites);
Meteor.users.attachSchema(Schemas.user);