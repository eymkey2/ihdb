Template.book.helpers({

	'getHid': function () {
		return this.hotel.hid;
	},

	'roomsReady': function () {
		if( this.roomsUpdated.get().length !== 0) {
			return true;
		}

		return false;
	}

 });