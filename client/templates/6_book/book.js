Template.book.events({

	'submit #datepicker': function (event, template) {
		event.preventDefault();

		var ctx = this;

		ctx.roomsUpdated.set([]);
		ctx.selectedPeriod.set();

		ctx.checkIn.set(new Date(template.find('#checkIn').value.trim()));
		ctx.checkOut.set(new Date(template.find('#checkOut').value.trim()));

		Meteor.call('getSelectedPeriod', {
			checkIn: ctx.checkIn.get(),
			checkOut: ctx.checkOut.get()
		}, function (error, result) {
			if( error ){
				console.log('error getSelectedPeriod: ', error);
			}
			else {
				ctx.selectedPeriod.set(result);
			}
		});

		ctx.rooms.fetch().forEach( function (room) {

			Meteor.call( 'isRoomAvailable',
				{
					rid: room._id,
					checkIn: ctx.checkIn.get(),
					checkOut: ctx.checkOut.get(),
					quantity: 1,
					returnRoomsAvailable: true
				},

				function (error, result) {
					if( error ){
						console.log('error isRoomAvailable: ', error);
					}
					else {
						var max = result[Object.keys(result)[0]];
						for( var i in result ){
							if ( result[i] < max ) {
								max = result[i];
							}
						}

						room.amount = max;
						var roomsUpdated = ctx.roomsUpdated.get();
						roomsUpdated.push(room);

						ctx.roomsUpdated.set(roomsUpdated);
					}
				}
			);

		});
	},

	'submit #roompicker, change #roompicker': function (event, template) {
		event.preventDefault();

		var ctx = this;
		var form = template.find('#roompicker');

		var rooms = [];
		for ( var i = 0; i < form.elements.length; i++){
			if(form.elements[i].checked){

				var rid = form.elements[i].getAttribute('data-rid');
				var quantity = Number(template.find('#' + rid).value.trim()) || 0;

				rooms.push({rid: rid, quantity: quantity});
			}
		}
		ctx.selectedRooms.set(rooms);

		Meteor.call( 'getPriceForBooking',
			{
				rooms: rooms,
				checkIn: ctx.checkIn.get(),
				checkOut: ctx.checkOut.get()
			},

			function (error, total) {
				if( error ){
					console.log('error getPriceForBooking: ', error);
				}
				else {
					ctx.total.set(total);
				}
			}
		);
	},

	'submit #paymentform': function (event, template) {
		event.preventDefault();

		var ctx = this;

		var creditCard = {
    	"type": template.find('#type').value.trim(),
      "number": template.find('#number').value.trim(),
      "expire_month": template.find('#expire_month').value.trim(),
      "expire_year": template.find('#expire_year').value.trim(),
      "cvv2": template.find('#cvv2').value.trim(),
      "first_name": template.find('#first_name').value.trim(),
      "last_name": template.find('#last_name').value.trim()	
		};
		
		Meteor.call('addBooking',
		{
			rooms: ctx.selectedRooms.get(),
			checkIn: ctx.checkIn.get(),
			checkOut: ctx.checkOut.get(),
			hid: ctx.hotel.hid
		}, creditCard, function (error, result){
			console.log(error, result);
		});
	}

});
