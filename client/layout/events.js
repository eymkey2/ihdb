Template.layout.events({

	'submit #searchByHid': function (event, template) {
		event.preventDefault();

		var hid = template.find('#searchByHid input[type="search"]').value.trim();
		Router.go('/view/hotel/' + hid);
	}

});