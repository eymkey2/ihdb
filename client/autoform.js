//
//	add
//

AutoForm.addHooks(['addHotelForm'],
	{
		onSuccess: function (formType, result) {

			Meteor.call('getHidById', {id: result}, function (error, hid) {
				if ( error ) {
					console.log('error: add hotel', error);
				}
				else {
					console.log('success: add hotel');
					Router.go('/view/hotel/' + hid);
				}
			});

		}
	}
);

AutoForm.addHooks(['addRoomForm'],
	{

		before: {
			method: function (doc) {
				doc.owner = Router.current().getParams().hid;

				return doc;
			}
		},

		onSuccess: function (formType, result) {
			var params = Router.current().getParams();

			console.log('success: add room');
			Router.go('/view/hotel/' + params.hid);
		},

		onError: function (formType, error) {
			console.log('error: add room', error);
		}

	}
);





//
//	edit
//

AutoForm.addHooks(['editHotelForm'], 
	{

		before: {
			method: function(doc) {
				doc.hid = Router.current().getParams().hid;

				return doc;
			}
		},

		onSuccess: function (formType, result) {
			var params = Router.current().getParams();

			console.log('success: edit hotel');
			Router.go('/view/hotel/' + params.hid);
		},

		onError: function (formType, error) {
			console.log('error: edit hotel', error);
		}

	}
);

AutoForm.addHooks(['editRoomForm'], 
	{

		before: {

			method: function (doc) {
				doc.rid = Router.current().getParams().rid;

				return doc;
			}
		},

		onSuccess: function (formType, result) {
			var params = Router.current().getParams();

			console.log('success: edit room');
			Router.go('/view/hotel/' + params.hid + '/room/' + params.rid);
		},

		onError: function (formType, error) {
			console.log('error: edit room', error);
		}

	}
);