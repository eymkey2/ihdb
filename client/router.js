Router.configure(
	{
		layoutTemplate: 'layout'
	}
);





// require login
Router.onBeforeAction( function () {
  if( !Meteor.userId() ){
    this.redirect( '/landingpage' );
    this.next();
  }
  else {
    this.next();
  }
});

// redirect to dashboard or landingpage
Router.route( '/', function () {

	this.redirect( 'dashboard' );
});

// dashboard
Router.route( '/dashboard', {
	subscriptions: function(){
		return [
			Meteor.subscribe('hotels'),
			Meteor.subscribe('favorites')
		];
	},

	data: function () {
		return {
			title: 'Dashboard',

			hotels: Hotels.find(),
			favorites: Favorites.find(),

			buttons: [{href: '/add/hotel', name: 'add hotel'}]
		}
	},

	action: function(){
		this.render('dashboard');
	}
});

// landingpage
Router.route( '/landingpage', function () {
	
	// redirect user to dashboard, if logged in
	if( Meteor.userId() ){
		this.redirect('/');
	}

	this.render( 'landingpage' );
});





//
//	add
//

// hotel
Router.route( '/add/hotel', {
	data: function () {
		return {
			title: 'add hotel',
			
			id: 'addHotelForm',
			schema: 'Schemas.addHotel',
			method: 'addHotel',
			omit: '',

			buttons: [
				{href: '/', name: 'back'}
			]
		}
	},

	action: function () {
		this.render( 'add' );
	}
});

// room
Router.route( '/add/room/:hid', {
	data: function () {
		return {
			title: 'add room',

			id: 'addRoomForm',
			schema: 'Schemas.addRoom',
			method: 'addRoom',
			omit: 'owner',

			buttons: [
				{href: '/view/hotel/' + this.params.hid, name: 'back'}
			]
		}
	},

	action: function () {
		this.render( 'add' );
	}
});

// favorite
Router.route( 'add/favorite/:hid', function () {
	Meteor.call( 'addFavorite', {hid: this.params.hid}, function (error) {
		if( error ){
			console.log( 'error: add favorite', error );
		}
		else {
			console.log( 'success: add favorite' );
		}
	});

	this.redirect( '/view/hotel/' + this.params.hid );
});





//
//	view
//

// hotel
Router.route( '/view/hotel/:hid', {
	subscriptions: function () {
		return [
			Meteor.subscribe('hotel', this.params.hid),
			Meteor.subscribe('favorites')
		]
	},

	data: function () {

		if( this.ready() ){
			var hotel = Hotels.findOne();
			var isFavorite = Favorites.findOne( {hid: hotel.hid} );

			// select buttons
			var buttons = [];
			buttons.push( {href: '/book/' + hotel.hid, name: 'book'} );
			
			if( hotel.owner === Meteor.userId() ){
				buttons.push( {href: '/add/room/' + hotel.hid, name: 'add room'} );
				buttons.push( {href: '/edit/hotel/' + hotel.hid, name: 'edit'} );
				buttons.push( {href: '/remove/hotel/' + hotel.hid, name: 'remove'} );
			}
			else {
				if( isFavorite ){
					buttons.push( {href: '/remove/favorite/' + this.params.hid, name: 'unfavorite'} );
				}
				else {
					buttons.push( {href: '/add/favorite/' + this.params.hid, name: 'favorite'} );
				}
			}

			buttons.push({href: '/', name: 'back'});

			return {
				hotel: hotel,
				rooms: Rooms.find(),
				buttons: buttons
			}
		}

	},

	action: function () {
		this.render( 'viewHotel' );
	}
});

// room
Router.route( '/view/hotel/:hid/room/:rid', {
	subscriptions: function () {
		return Meteor.subscribe('hotel', this.params.hid);
	},

	data: function () {

		if( this.ready() ){

			var hotel = Hotels.findOne();
			var room = Rooms.findOne(Router.current().getParams().rid);

			var buttons = [];
			buttons.push({href: '/book/' + hotel.hid, name: 'book'});

			if( hotel.owner === Meteor.userId() ){
				buttons.push({href: '/edit/hotel/' + hotel.hid + '/room/' + room._id, name: 'edit'});
				buttons.push({href: '/remove/hotel/' + hotel.hid + '/room/' + room._id, name: 'remove'});
			}

			buttons.push({href: '/view/hotel/' + hotel.hid, name: 'back'});

			return {
				hotel: hotel,
				room: room,
				buttons: buttons
			}
		}

	},

	action: function(){
		this.render('viewRoom');
	}
});





//
//	edit
//

// hotel
Router.route( '/edit/hotel/:hid', {
	subscriptions: function(){
		return Meteor.subscribe('hotel', this.params.hid);
	},

	data: function () {

		if( this.ready() ){
			var hotel = Hotels.findOne();

			return {
				title: 'edit ' + hotel.name,

				id: 'editHotelForm',
				doc: hotel,
				schema: 'Schemas.updateHotel',
				method: 'updateHotel',
				omit: 'hid',

				buttons: [{href:'/view/hotel/' + hotel.hid, name: 'back'}]
			}
		}

	},

	action: function () {
		if ( this.ready() ){
			this.render('edit');
		}
		else {
			this.next();
		}
	}
});

// room
Router.route( '/edit/hotel/:hid/room/:rid', {
	subscriptions: function(){
		return Meteor.subscribe('hotel', this.params.hid);
	},

	data: function () {

		if ( this.ready() ) {
			var room = Rooms.findOne(this.params.rid);

			return {
				title: 'edit ' + room.name,

				id: 'editRoomForm',
				doc: room,
				schema: 'Schemas.updateRoom',
				method: 'updateRoom',
				omit: 'rid',

				buttons: [{href:'/view/hotel/' + Router.current().getParams().hid + '/room/' + room._id, name: 'back'}]
			}
		}

	},

	action: function () {
		if( this.ready() ){
			this.render('edit');
		}
		else {
			this.next();
		}
	}
});





//
//	remove
//

// hotel
Router.route( 'remove/hotel/:hid', function () {
	Meteor.call('removeHotel', {hid: this.params.hid}, function (error) {
		if ( error ){
			console.log(error);
		}
	});
	
	this.redirect('/');
});

// room
Router.route( 'remove/hotel/:hid/room/:rid', function () {
	Meteor.call( 'removeRoom', {rid: this.params.rid}, function (error) {
		if( error ) {
			console.log(error);
		}
	});

	this.redirect( '/view/hotel/' + this.params.hid );
});

// favorite
Router.route( 'remove/favorite/:hid', function () {
	Meteor.call('removeFavorite', {hid: this.params.hid}, function (error) {
		if( error ){
			console.log('error: remove favorite', error);
		}
		else {
			console.log('success: remove favorite');
		}
	});

	this.redirect('/view/hotel/' + this.params.hid);
});





//
//	booking flow
//

Router.route( '/book/:hid', {
	subscriptions: function () {
		return Meteor.subscribe('hotel', this.params.hid);
	},

	data: function () {
		return {
			hotel: Hotels.findOne(),
			rooms: Rooms.find({public: true}),
			roomsUpdated: new ReactiveVar([]),

			selectedRooms: new ReactiveVar([]),
			checkIn: new ReactiveVar(),
			checkOut: new ReactiveVar(),
			selectedPeriod: new ReactiveVar(),

			total: new ReactiveVar(0),

			buttons: [{href: '/view/hotel/' + this.params.hid, name: 'back'}]
		}
	},

	action: function () {
		this.render('book');
	}
});
